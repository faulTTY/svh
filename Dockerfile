FROM python:3.9

COPY requirements.txt /
RUN pip install -r requirements.txt

COPY src/aggregates.py /usr/local/lib/python3.9/site-packages/sql_util/

COPY . /app
WORKDIR /app

COPY src/init.sh /usr/local/bin/

CMD [ "gunicorn", "--bind", "0.0.0.0:5000", "svhoerne.wsgi" ]