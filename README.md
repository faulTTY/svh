# Website of sports club SV Hörne 01 e.V.

# Introduction
This is the source code for the website of the german sports club SV Hörne 01 e.V. and provides a dockerized version to use as-is.


# Installation instructions

## Requirements
- `git`
- `docker`
- `docker-compose`

## Steps

### Step 1 - Clone directory
Clone this repository to your server:
```
git clone https://gitlab.com/faulTTY/svh.git
```


### Step 2 - Create necessary files for the project
Enter the directory and create a `creds` subdirectory:
```
cd svh && mkdir creds
```

Create a `creds/creds.py` file in and fill with environment specific settings

```
SECRET_KEY = "MYTOTALLYSECRETKEY"
DEBUG = False
ALLOWED_HOSTS = ["0.0.0.0", "localhost"]
CSRF_TRUSTED_ORIGINS = ["https://example.org"]

# Email
EMAIL_HOST = "mail.example.org"
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_USER = "example@mail.example.org"
EMAIL_HOST_PASSWORD = "myPass"
RECIPIENT_ADDRESS = "info@mail.example.org"

```

Create your own or use example docker-compose.yaml:
```
version: "3.7"
services:
  web:
    build: .
    command: sh -c "/usr/local/bin/init.sh && exec gunicorn --bind 0.0.0.0:5000 svhoerne.wsgi"
    ports:
      - "5000:5000"
    volumes:
      - staticfiles:/app/staticfiles
      - db:/app/db
    restart: unless-stopped
volumes:
  db:
  staticfiles:

```

### Step 3 - Run

Start the process:
```
docker-compose up -d
```

Finished. You should have a running website at whatever port you specified in the docker-compose, default 5000.

# Credits

SV Hörne 01 e.V