from django.contrib import admin

# Register your models here.
from .models import (
    Team,
    Season,
    PlayingSchedule,
    ScheduledMatch,
    Player,
    News,
    MatchReport,
    PlayerMatchResult,
    Documents,
)


class DocumentsAdmin(admin.ModelAdmin):
    list_display = ("name", "document", "showInFooter")


class TeamAdmin(admin.ModelAdmin):
    list_display = ("long_name", "short_name", "penalty_points")


class ScheduledMatchAdmin(admin.ModelAdmin):
    list_display = (
        "match_day",
        "schedule",
        "home_team",
        "guest_team",
        "play_time",
        "goal_count_home_team",
        "goal_count_guest_team",
    )
    list_filter = ("schedule", "play_time", "home_team", "guest_team")


class MatchResultAdmin(admin.ModelAdmin):
    list_display = ("scheduledMatch", "goal_count_home_team", "goal_count_guest_team")


class NewsAdmin(admin.ModelAdmin):
    list_display = ("title", "pub_date")
    list_filter = ("pub_date",)


class SeasonAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "win",
        "draw",
        "defeat",
        "count_promoted",
        "count_relegated",
        "favourite_team",
        "year",
    )


class MatchReportAdmin(admin.ModelAdmin):
    list_display = ("scheduledMatch", "title", "pub_date")
    list_filter = ("scheduledMatch__schedule", "pub_date")


class PlayerMatchResultAdmin(admin.ModelAdmin):
    list_display = (
        "playerMatchResultId",
        "player",
        "scheduledMatch",
        "goals",
        "assists",
        "yellow_cards",
        "yellow_red_cards",
        "red_cards",
    )
    list_filter = ("scheduledMatch__schedule",)


class PlayingScheduleAdmin(admin.ModelAdmin):
    list_display = ("title", "season")
    list_filter = ("season",)


admin.site.register(Team, TeamAdmin)
admin.site.register(Season, SeasonAdmin)
admin.site.register(PlayingSchedule, PlayingScheduleAdmin)
admin.site.register(ScheduledMatch, ScheduledMatchAdmin)
admin.site.register(Player)
admin.site.register(News, NewsAdmin)
admin.site.register(MatchReport, MatchReportAdmin)
admin.site.register(PlayerMatchResult, PlayerMatchResultAdmin)
admin.site.register(Documents, DocumentsAdmin)
