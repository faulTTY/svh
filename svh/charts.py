import pygal
from django.db.models import OuterRef, Subquery, Sum, Count, Q, F
from django.db.models import Avg, Max, Min, Sum
from sql_util.utils import SubqueryCount, SubquerySum, SubqueryAggregate

from .models import (
    News,
    ScheduledMatch,
    MatchReport,
    Player,
    PlayingSchedule,
    Season,
    Team,
)


class GoalsPerDayLineChart:
    def __init__(self, scheduleId, **kwargs):
        self.chart = pygal.Line(**kwargs)
        self.chart.title = "Anzahl geschossene Tore"
        self.scheduleId = scheduleId

    def get_data(self):
        showSchedule = self.scheduleId

        maximum = ScheduledMatch.objects.filter(
            Q(schedule__playingScheduleId=showSchedule) & ~Q(goal_count_home_team=None)
        ).aggregate(Max("match_day"))

        data = {}

        for team in Team.objects.all():
            data[team.long_name] = [0] * (maximum["match_day__max"])
            for match in ScheduledMatch.objects.filter(
                (
                    Q(home_team_id=team.teamId)
                    & ~Q(goal_count_home_team=None)
                    & Q(schedule__playingScheduleId=showSchedule)
                )
            ).order_by("match_day"):
                data[team.long_name][match.match_day - 1] = match.goal_count_home_team

            for match in ScheduledMatch.objects.filter(
                (
                    Q(guest_team_id=team.teamId)
                    & ~Q(goal_count_guest_team=None)
                    & Q(schedule__playingScheduleId=showSchedule)
                )
            ).order_by("match_day"):
                data[team.long_name][match.match_day - 1] = match.goal_count_guest_team
        return data

    def generate(self):
        showSchedule = self.scheduleId

        maximum = ScheduledMatch.objects.filter(
            Q(schedule__playingScheduleId=showSchedule) & ~Q(goal_count_home_team=None)
        ).aggregate(Max("match_day"))

        self.chart.x_labels = range(1, maximum["match_day__max"])

        # Get chart data
        chart_data = self.get_data()

        # Add data to chart
        for team in chart_data:
            if chart_data[team]:
                self.chart.add(team, chart_data[team])

        # Return the rendered SVG
        return self.chart.render_data_uri()
