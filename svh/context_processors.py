def documents(request):
    from svh.models import Documents

    return {"documents": Documents.objects.all()}
