from django import forms
from django.conf import settings
from django.core.mail import send_mail
from django.core.validators import EmailValidator


class ContactForm(forms.Form):

    name = forms.CharField(
        label="Vor- und Nachname",
        max_length=120,
    )
    email = forms.CharField(
        label="E-Mail", widget=forms.EmailInput(attrs={"class": "validate"})
    )
    inquiry = forms.CharField(label="Betreff", max_length=70)
    message = forms.CharField(
        label="Nachricht",
        widget=forms.Textarea(attrs={"class": "materialize-textarea"}),
        max_length=5000,
    )

    def get_info(self):
        """
        Method that returns formatted information
        :return: subject, msg
        """
        # Cleaned data
        cl_data = super().clean()

        name = cl_data.get("name").strip()
        from_email = cl_data.get("email")
        subject = cl_data.get("inquiry")

        msg = f"{name} mit E-Mail {from_email} schrieb:"
        msg += f'\n"{subject}"\n\n'
        msg += cl_data.get("message")

        return subject, msg

    def send(self):

        subject, msg = self.get_info()

        send_mail(
            subject=subject,
            message=msg,
            from_email=settings.EMAIL_HOST_USER,
            recipient_list=[settings.RECIPIENT_ADDRESS],
        )
