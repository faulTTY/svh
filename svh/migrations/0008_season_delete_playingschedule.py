# Generated by Django 4.0.1 on 2022-01-24 13:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('svh', '0007_playingschedule_count_promoted_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='Season',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('title', models.CharField(help_text='Titel der Liga', max_length=200, verbose_name='Titel')),
                ('win', models.IntegerField(default=3, help_text='Anzahl der Punkte bei Sieg', verbose_name='Sieg')),
                ('draw', models.IntegerField(default=1, help_text='Anzahl der Punkte bei Unentschieden', verbose_name='Unentschieden')),
                ('defeat', models.IntegerField(default=0, help_text='Anzahl der Punkte bei Niederlage', verbose_name='Niederlage')),
                ('count_promoted', models.IntegerField(default=3, help_text='Wieviele Aufsteiger gibt es?', verbose_name='Anzahl Aufsteiger')),
                ('count_relegated', models.IntegerField(default=3, help_text='Wieviele Absteiger gibt es?', verbose_name='Anzahl Absteiger')),
                ('favourite_team', models.ForeignKey(default=1, help_text='Lieblingsmannschaft für diverse Voreinstellungen', on_delete=django.db.models.deletion.CASCADE, to='svh.team')),
            ],
        ),
        migrations.DeleteModel(
            name='PlayingSchedule',
        ),
    ]
