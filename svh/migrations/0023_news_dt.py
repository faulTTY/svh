# Generated by Django 4.0.1 on 2022-01-24 15:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('svh', '0022_news'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='dt',
            field=models.DateTimeField(auto_now=True, help_text='Zeitstempel der Nachricht', verbose_name='Zeitstempel'),
        ),
    ]
