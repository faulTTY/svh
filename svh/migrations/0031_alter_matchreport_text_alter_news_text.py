# Generated by Django 4.0.1 on 2022-01-25 11:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('svh', '0030_rename_goal_count_foreign_team_gameresult_goal_count_guest_team'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matchreport',
            name='text',
            field=models.CharField(help_text='Text des Spielberichts', max_length=5000, verbose_name='Text'),
        ),
        migrations.AlterField(
            model_name='news',
            name='text',
            field=models.CharField(help_text='Text der Nachricht', max_length=5000, verbose_name='Text'),
        ),
    ]
