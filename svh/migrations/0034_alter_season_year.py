# Generated by Django 4.0.1 on 2022-01-29 12:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('svh', '0033_season_year'),
    ]

    operations = [
        migrations.AlterField(
            model_name='season',
            name='year',
            field=models.DateField(help_text='Das Jahr des ersten Spieltags der Saison', verbose_name='Jahr'),
        ),
    ]
