# Generated by Django 4.0.1 on 2022-02-20 16:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('svh', '0050_alter_scheduledmatch_goal_count_guest_team_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scheduledmatch',
            name='goal_count_guest_team',
            field=models.IntegerField(blank=True, help_text='Anzahl der Tore der Auswärtsmannschaft?', null=True, verbose_name='Anzahl Tore Auswärtsmannschaft'),
        ),
        migrations.AlterField(
            model_name='scheduledmatch',
            name='goal_count_home_team',
            field=models.IntegerField(blank=True, help_text='Anzahl der Tore der Heimmannschaft?', null=True, verbose_name='Anzahl Tore Heimmannschaft'),
        ),
    ]
