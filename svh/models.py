from operator import mod
from pyexpat import model
from typing import Any
from django.db import models
from tinymce.models import HTMLField

# Create your models here.
class Team(models.Model):
    teamId = models.BigAutoField(primary_key=True)
    long_name = models.CharField(
        "Name", max_length=200, help_text="Vollständiger Name der Mannschaft"
    )
    short_name = models.CharField(
        "Kürzel", max_length=5, help_text="Abgekürzter Name in Großbuchstaben"
    )
    penalty_points = models.IntegerField(
        "Strafpunkte", default=0, help_text="Anzahl der Strafpunkte"
    )

    logo = models.ImageField("Logo", upload_to="logos/%Y%m%d", blank=True)

    class Meta:
        verbose_name = "Mannschaft"
        verbose_name_plural = "Mannschaften"

    def __str__(self):
        return self.long_name


class Season(models.Model):
    seasonId = models.BigAutoField(primary_key=True)
    title = models.CharField("Titel", max_length=200, help_text="Titel der Saison")
    win = models.IntegerField("Sieg", default=3, help_text="Anzahl der Punkte bei Sieg")
    draw = models.IntegerField(
        "Unentschieden", default=1, help_text="Anzahl der Punkte bei Unentschieden"
    )
    defeat = models.IntegerField(
        "Niederlage", default=0, help_text="Anzahl der Punkte bei Niederlage"
    )
    count_promoted = models.PositiveIntegerField(
        "Anzahl Aufsteiger", default=3, help_text="Wieviele Aufsteiger gibt es?"
    )
    count_relegated = models.PositiveIntegerField(
        "Anzahl Absteiger", default=3, help_text="Wieviele Absteiger gibt es?"
    )
    favourite_team = models.ForeignKey(
        Team,
        default=1,
        help_text="Lieblingsmannschaft für diverse Voreinstellungen",
        on_delete=models.CASCADE,
        verbose_name="Lieblingsmannschaft",
    )

    year = models.DateField(
        "Jahr",
        help_text="Das Jahr des ersten Spieltags der Saison",
    )

    class Meta:
        verbose_name = "Saison"
        verbose_name_plural = "Saisons"

    def __str__(self):
        return self.title


class PlayingSchedule(models.Model):
    playingScheduleId = models.BigAutoField(primary_key=True)
    title = models.CharField("Titel", max_length=200, help_text="Titel des Spielplans")
    season = models.ForeignKey(
        Season,
        help_text="Spielplan gehört zu welcher Saison?",
        on_delete=models.CASCADE,
        verbose_name="Saison",
    )
    current_matchday = models.PositiveSmallIntegerField(
        "Aktueller Spieltag",
        help_text="Trage hier den aktuellen Spieltag ein, der standardmäßig angezeigt werden soll.",
        default=1,
    )

    class Meta:
        verbose_name = "Spielplan"
        verbose_name_plural = "Spielpläne"

    def __str__(self):
        return self.title


class ScheduledMatch(models.Model):
    scheduledMatchId = models.BigAutoField(primary_key=True)
    schedule = models.ForeignKey(
        PlayingSchedule,
        help_text="Spiel gehört zu welchem Spielplan?",
        on_delete=models.CASCADE,
        verbose_name="Spielplan",
    )
    home_team = models.ForeignKey(
        Team,
        related_name="homeTeam",
        help_text="Heimmannschaft",
        on_delete=models.CASCADE,
        verbose_name="Heimmannschaft",
    )
    guest_team = models.ForeignKey(
        Team,
        related_name="guestTeam",
        help_text="Gastmannschaft",
        on_delete=models.CASCADE,
        verbose_name="Gastmannschaft",
    )
    play_time = models.DateTimeField("Spielzeit", help_text="Zeitpunkt des Spiels")
    match_day = models.PositiveIntegerField(
        "Spieltag", help_text="Nummer des Spieltags", default=1
    )

    goal_count_home_team = models.IntegerField(
        "Anzahl Tore Heimmannschaft",
        help_text="Anzahl der Tore der Heimmannschaft?",
        blank=True,
        null=True,
    )
    goal_count_guest_team = models.IntegerField(
        "Anzahl Tore Auswärtsmannschaft",
        help_text="Anzahl der Tore der Auswärtsmannschaft?",
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = "Geplantes Spiel"
        verbose_name_plural = "Geplante Spiele"

    def __str__(self):
        return "{}, Spieltag {}, {} : {}".format(
            self.schedule, self.match_day, self.home_team, self.guest_team
        )


class MatchReport(models.Model):
    matchReportId = models.BigAutoField(primary_key=True)
    scheduledMatch = models.ForeignKey(
        ScheduledMatch,
        help_text="Spielbericht gehört zu welchem Spiel?",
        on_delete=models.CASCADE,
    )
    title = models.CharField(
        "Titel", max_length=30, help_text="Titel des Spielberichts"
    )
    text = HTMLField("Text", max_length=5000, help_text="Text des Spielberichts")
    pub_date = models.DateTimeField(
        "Zeitstempel", help_text="Zeitstempel des Spielberichts"
    )

    class Meta:
        verbose_name = "Spielbericht"
        verbose_name_plural = "Spielberichte"

    def __str__(self):
        return self.title


class Player(models.Model):
    playerId = models.BigAutoField(primary_key=True)
    name = models.CharField("Name", max_length=200, help_text="Name des Spielers")

    class Meta:
        verbose_name = "Spieler"
        verbose_name_plural = "Spieler"

    def __str__(self):
        return self.name


class PlayerMatchResult(models.Model):
    playerMatchResultId = models.BigAutoField(primary_key=True, verbose_name="Id")
    player = models.ForeignKey(
        Player,
        help_text="Ergebnis gehört zu welchem Spieler?",
        on_delete=models.CASCADE,
        verbose_name="Spieler",
    )
    scheduledMatch = models.ForeignKey(
        ScheduledMatch,
        help_text="Ergebnis gehört zu welchem Spielergebnis?",
        on_delete=models.CASCADE,
        default=5,
        verbose_name="Geplantes Spiel",
    )
    goals = models.IntegerField(
        "Tore", default=0, help_text="Anzahl der geschossenen Tore"
    )
    assists = models.IntegerField(
        "Vorlagen", default=0, help_text="Anzahl der gemachten Vorlagen"
    )
    yellow_cards = models.IntegerField(
        "Gelbe Karten", default=0, help_text="Anzahl der bekommenen gelben Karten"
    )
    yellow_red_cards = models.IntegerField(
        "Gelb-Rote Karten",
        default=0,
        help_text="Anzahl der bekommenen Gelb-Roten Karten",
    )
    red_cards = models.IntegerField(
        "Rote Karten", default=0, help_text="Anzahl der bekommenen roten Karten"
    )

    class Meta:
        verbose_name = "Spielerergebnis"
        verbose_name_plural = "Spielerergebnisse"

    def __str__(self):
        return "{}, {}".format(self.player, self.scheduledMatch)


class News(models.Model):
    newsId = models.BigAutoField(primary_key=True)
    title = models.CharField("Titel", max_length=30, help_text="Titel der Nachricht")
    text = models.TextField("Text", max_length=5000, help_text="Text der Nachricht")
    pub_date = models.DateTimeField(
        "Zeitstempel", help_text="Zeitstempel der Nachricht"
    )

    class Meta:
        verbose_name = "Neuigkeit"
        verbose_name_plural = "Neuigkeiten"

    def __str__(self):
        return self.title


class Documents(models.Model):
    documentsId = models.BigAutoField(primary_key=True)
    name = models.CharField(
        "Titel", max_length=30, help_text="Titel der Dokumentensammlung"
    )
    document = models.FileField("Dokument", upload_to="documents/%Y%m%d")
    showInFooter = models.BooleanField(
        "Zeige im Footer?",
        default=False,
        help_text="Soll dieses Dokument automatisch auf jeder Seite im Footer verlinkt dargestellt werden?",
    )

    class Meta:
        verbose_name = "Dokument"
        verbose_name_plural = "Dokumente"

    def __str__(self):
        return self.name
