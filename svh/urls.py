from unicodedata import name
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic import TemplateView
from .views import (
    ResultsListView,
    ReportListView,
    ReportDetailView,
    PlayerListView,
    PlayingScheduleListView,
    StatsListView,
    ContactView,
    ContactSuccessView,
)

from . import views

app_name = "svh"
urlpatterns = [
    path("", views.index, name="index"),
    path(
        "results/",
        ResultsListView.as_view(template_name="svh/matchresult_list.html"),
        name="results",
    ),
    path("reports/", ReportListView.as_view(), name="reports"),
    path("reports/<pk>/", ReportDetailView.as_view(), name="report-detail"),
    path("players/", PlayerListView.as_view(), name="players"),
    path("schedule/", PlayingScheduleListView.as_view(), name="schedule"),
    path("stats/", StatsListView.as_view(template_name="svh/stats.html"), name="stats"),
    path("about/", TemplateView.as_view(template_name="svh/about.html")),
    path("contact/", ContactView.as_view(), name="contact"),
    path("success/", ContactSuccessView.as_view(), name="success"),
    path("tinymce/", include("tinymce.urls")),
    path("impressum/", TemplateView.as_view(template_name="svh/impressum.html")),
    path("datenschutz/", TemplateView.as_view(template_name="svh/datenschutz.html")),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
