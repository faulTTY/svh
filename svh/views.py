from audioop import reverse
from difflib import Match
from urllib import request
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.db.models import OuterRef, Subquery, Sum, Count, Q, F, Max
from sql_util.utils import SubqueryCount, SubquerySum, SubqueryAggregate
import datetime

from django.views.generic import FormView, TemplateView
from .forms import ContactForm
from django.urls import reverse_lazy

from .models import (
    News,
    ScheduledMatch,
    MatchReport,
    Player,
    PlayingSchedule,
    Season,
    Team,
)

from pygal.style import DarkStyle, DefaultStyle, CleanStyle
from .charts import GoalsPerDayLineChart


def index(request):
    if not Season.objects.all():
        return render(request, "svh/index.html", {})
    favouriteTeam = Season.objects.latest("year").favourite_team
    context = {}
    now = datetime.datetime.now()
    now = now.replace(tzinfo=datetime.timezone.utc)
    context["news"] = News.objects.filter(pub_date__lte=now).order_by("-pub_date")[:3]

    next_game = ScheduledMatch.objects.filter(
        (Q(home_team=favouriteTeam.teamId) | Q(guest_team=favouriteTeam.teamId))
        & Q(play_time__gte=now)
    ).order_by("play_time")

    if len(next_game) > 0:
        next_game = next_game[0]

        context["next_game"] = next_game

        context["games_against"] = ScheduledMatch.objects.filter(
            (Q(home_team=next_game.home_team) | Q(home_team=next_game.guest_team))
            & (Q(guest_team=next_game.home_team) | Q(guest_team=next_game.guest_team))
        ).filter(play_time__lt=now)

    context["favTeam"] = favouriteTeam

    return render(request, "svh/index.html", context)


class ResultsListView(ListView):
    model = ScheduledMatch

    def get_queryset(self):
        scheduleId = self.request.GET.get("scheduleId")
        if not scheduleId:
            showSchedule = PlayingSchedule.objects.latest(
                "season__year"
            ).playingScheduleId
        else:
            showSchedule = scheduleId

        untilMatchday = self.request.GET.get("untilMatchday")
        if not untilMatchday:
            showMatchday = ScheduledMatch.objects.latest("match_day").match_day
        else:
            showMatchday = untilMatchday

        teamsWithGamesInCurrentSchedule = ScheduledMatch.objects.filter(
            schedule_id=showSchedule
        )
        teamSet = set()
        for te in teamsWithGamesInCurrentSchedule:
            teamSet.add(te.home_team.teamId)

        allTeams = Team.objects.all()
        allTeamsIds = allTeams.values_list("pk", flat=True)

        excludeTeamIds = list(set(allTeamsIds) - teamSet)

        queryset = (
            Team.objects.exclude(teamId__in=excludeTeamIds)
            .annotate(
                matches=SubqueryCount(
                    "homeTeam",
                    filter=~Q(
                        goal_count_home_team=None,
                    )
                    & Q(schedule__playingScheduleId=showSchedule)
                    & Q(match_day__lte=showMatchday),
                )
                + SubqueryCount(
                    "guestTeam",
                    filter=~Q(goal_count_guest_team=None)
                    & Q(schedule__playingScheduleId=showSchedule)
                    & Q(match_day__lte=showMatchday),
                )
            )
            .annotate(
                matchesWon=SubqueryCount(
                    "homeTeam",
                    filter=Q(goal_count_home_team__gt=F("goal_count_guest_team"))
                    & Q(schedule__playingScheduleId=showSchedule)
                    & Q(match_day__lte=showMatchday),
                )
                + SubqueryCount(
                    "guestTeam",
                    filter=Q(goal_count_guest_team__gt=F("goal_count_home_team"))
                    & Q(schedule__playingScheduleId=showSchedule)
                    & Q(match_day__lte=showMatchday),
                )
            )
            .annotate(
                matchesLost=SubqueryCount(
                    "homeTeam",
                    filter=Q(goal_count_home_team__lt=F("goal_count_guest_team"))
                    & Q(schedule__playingScheduleId=showSchedule)
                    & Q(match_day__lte=showMatchday),
                )
                + SubqueryCount(
                    "guestTeam",
                    filter=Q(goal_count_guest_team__lt=F("goal_count_home_team"))
                    & Q(schedule__playingScheduleId=showSchedule)
                    & Q(match_day__lte=showMatchday),
                )
            )
            .annotate(
                matchesDraw=SubqueryCount(
                    "homeTeam",
                    filter=Q(goal_count_home_team=F("goal_count_guest_team"))
                    & Q(schedule__playingScheduleId=showSchedule)
                    & Q(match_day__lte=showMatchday),
                )
                + SubqueryCount(
                    "guestTeam",
                    filter=Q(goal_count_guest_team=F("goal_count_home_team"))
                    & Q(schedule__playingScheduleId=showSchedule)
                    & Q(match_day__lte=showMatchday),
                )
            )
            .annotate(
                shotGoals=SubquerySum(
                    "homeTeam__goal_count_home_team",
                    filter=Q(schedule__playingScheduleId=showSchedule)
                    & Q(match_day__lte=showMatchday),
                )
                + SubquerySum(
                    "guestTeam__goal_count_guest_team",
                    filter=Q(schedule__playingScheduleId=showSchedule)
                    & Q(match_day__lte=showMatchday),
                )
            )
            .annotate(
                gotGoals=SubquerySum(
                    "homeTeam__goal_count_guest_team",
                    filter=Q(schedule__playingScheduleId=showSchedule)
                    & Q(match_day__lte=showMatchday),
                )
                + SubquerySum(
                    "guestTeam__goal_count_home_team",
                    filter=Q(schedule__playingScheduleId=showSchedule)
                    & Q(match_day__lte=showMatchday),
                )
            )
            .annotate(
                goalDifference=SubquerySum(
                    "homeTeam__goal_count_home_team",
                    filter=Q(schedule__playingScheduleId=showSchedule)
                    & Q(match_day__lte=showMatchday),
                )
                + SubquerySum(
                    "guestTeam__goal_count_guest_team",
                    filter=Q(schedule__playingScheduleId=showSchedule)
                    & Q(match_day__lte=showMatchday),
                )
                - (
                    SubquerySum(
                        "homeTeam__goal_count_guest_team",
                        filter=Q(schedule__playingScheduleId=showSchedule)
                        & Q(match_day__lte=showMatchday),
                    )
                    + SubquerySum(
                        "guestTeam__goal_count_home_team",
                        filter=Q(schedule__playingScheduleId=showSchedule)
                        & Q(match_day__lte=showMatchday),
                    )
                )
            )
            .annotate(
                points=(
                    SubqueryCount(
                        "homeTeam",
                        filter=Q(goal_count_home_team__gt=F("goal_count_guest_team"))
                        & Q(schedule__playingScheduleId=showSchedule)
                        & Q(match_day__lte=showMatchday),
                    )
                    + SubqueryCount(
                        "guestTeam",
                        filter=Q(goal_count_guest_team__gt=F("goal_count_home_team"))
                        & Q(schedule__playingScheduleId=showSchedule)
                        & Q(match_day__lte=showMatchday),
                    )
                )
                * PlayingSchedule.objects.filter(Q(playingScheduleId=showSchedule))[
                    0
                ].season.win
                + (
                    SubqueryCount(
                        "homeTeam",
                        filter=Q(goal_count_home_team=F("goal_count_guest_team"))
                        & Q(schedule__playingScheduleId=showSchedule)
                        & Q(match_day__lte=showMatchday),
                    )
                    + SubqueryCount(
                        "guestTeam",
                        filter=Q(goal_count_guest_team=F("goal_count_home_team"))
                        & Q(schedule__playingScheduleId=showSchedule)
                        & Q(match_day__lte=showMatchday),
                    )
                )
                * PlayingSchedule.objects.filter(Q(playingScheduleId=showSchedule))[
                    0
                ].season.draw
            )
        ).order_by("-points", "-goalDifference", "-shotGoals")

        return queryset

    def get_context_data(self, **kwargs):
        scheduleId = self.request.GET.get("scheduleId")
        if not scheduleId:
            showSchedule = PlayingSchedule.objects.latest(
                "season__year"
            ).playingScheduleId
        else:
            showSchedule = scheduleId

        untilMatchday = self.request.GET.get("untilMatchday")
        if not untilMatchday:
            # today = datetime.datetime.now()
            # today = today.replace(
            #     tzinfo=datetime.timezone.utc, hour=0, minute=0, second=0
            # )
            # showMatchday = (
            #     ScheduledMatch.objects.filter(play_time__gte=today)
            #     .order_by("play_time")[0]
            #     .match_day
            # )
            showMatchday = PlayingSchedule.objects.latest(
                "season__year"
            ).current_matchday
        else:
            showMatchday = untilMatchday

        context = super(ResultsListView, self).get_context_data(**kwargs)

        context["results"] = ScheduledMatch.objects.filter(
            Q(schedule_id=showSchedule)
            # & ~Q(goal_count_home_team=None)
            & Q(match_day=showMatchday)
        ).order_by("-match_day")

        context["seasons"] = Season.objects.order_by("-year")
        context["schedules"] = PlayingSchedule.objects.order_by("-season__year")

        matchday_list = (
            ScheduledMatch.objects.filter(Q(schedule_id=showSchedule))
            .values_list("match_day", flat=True)
            .order_by("-play_time")
        )
        context["matchdays"] = sorted(set(matchday_list), reverse=True)

        context["count_promoted"] = PlayingSchedule.objects.filter(
            Q(playingScheduleId=showSchedule)
        ).values_list("season__count_promoted", flat=True)

        context["count_relegated"] = PlayingSchedule.objects.filter(
            Q(playingScheduleId=showSchedule)
        ).values_list("season__count_relegated", flat=True)

        context["current_schedule"] = showSchedule
        context["current_matchday"] = showMatchday

        return context


class ReportListView(ListView):
    model = MatchReport

    def get_paginate_by(self, queryset):
        self.paginate_by = self.request.GET.get("paginate_by")
        if not self.paginate_by:
            self.paginate_by = 10
        return self.paginate_by

    def get_queryset(self):
        scheduleId = self.request.GET.get("scheduleId")
        if not scheduleId:
            showSchedule = PlayingSchedule.objects.latest(
                "season__year"
            ).playingScheduleId
        else:
            showSchedule = scheduleId

        queryset = MatchReport.objects.filter(
            Q(scheduledMatch__schedule__playingScheduleId=showSchedule)
        ).order_by("-pub_date")
        return queryset

    def get_context_data(self, **kwargs):
        scheduleId = self.request.GET.get("scheduleId")
        if not scheduleId:
            showSchedule = PlayingSchedule.objects.latest(
                "season__year"
            ).playingScheduleId
        else:
            showSchedule = scheduleId

        context = super(ReportListView, self).get_context_data(**kwargs)
        context["schedules"] = PlayingSchedule.objects.all()
        context["current_schedule"] = showSchedule
        return context


class PlayerListView(ListView):
    model = Player

    def get_paginate_by(self, queryset):
        self.paginate_by = self.request.GET.get("paginate_by")
        if not self.paginate_by:
            self.paginate_by = 10
        return self.paginate_by

    def get_queryset(self):
        scheduleId = self.request.GET.get("scheduleId")
        if not scheduleId:
            showSchedule = PlayingSchedule.objects.latest(
                "season__year"
            ).playingScheduleId
        else:
            showSchedule = scheduleId

        untilMatchday = self.request.GET.get("untilMatchday")
        if not untilMatchday:
            showMatchday = ScheduledMatch.objects.latest("match_day").match_day
        else:
            showMatchday = untilMatchday

        queryset = (
            (
                Player.objects.annotate(
                    goals=SubquerySum(
                        "playermatchresult__goals",
                        filter=Q(scheduledMatch__schedule=showSchedule)
                        & Q(scheduledMatch__match_day__lte=showMatchday),
                    )
                )
                .annotate(
                    assists=SubquerySum(
                        "playermatchresult__assists",
                        filter=Q(scheduledMatch__schedule=showSchedule)
                        & Q(scheduledMatch__match_day__lte=showMatchday),
                    )
                )
                .annotate(
                    yellow_cards=SubquerySum(
                        "playermatchresult__yellow_cards",
                        filter=Q(scheduledMatch__schedule=showSchedule)
                        & Q(scheduledMatch__match_day__lte=showMatchday),
                    )
                )
                .annotate(
                    yellow_red_cards=SubquerySum(
                        "playermatchresult__yellow_red_cards",
                        filter=Q(scheduledMatch__schedule=showSchedule)
                        & Q(scheduledMatch__match_day__lte=showMatchday),
                    )
                )
                .annotate(
                    red_cards=SubquerySum(
                        "playermatchresult__red_cards",
                        filter=Q(scheduledMatch__schedule=showSchedule)
                        & Q(scheduledMatch__match_day__lte=showMatchday),
                    )
                )
                .annotate(
                    games=Count(
                        "playermatchresult",
                        filter=Q(
                            playermatchresult__scheduledMatch__schedule=showSchedule
                        )
                        & Q(
                            playermatchresult__scheduledMatch__match_day__lte=showMatchday
                        ),
                    )
                )
            )
            .order_by("-games")
            .filter(Q(games__gt=0))
        )
        return queryset

    def get_context_data(self, **kwargs):
        scheduleId = self.request.GET.get("scheduleId")
        if not scheduleId:
            showSchedule = PlayingSchedule.objects.latest(
                "season__year"
            ).playingScheduleId
        else:
            showSchedule = scheduleId

        untilMatchday = self.request.GET.get("untilMatchday")
        if not untilMatchday:
            showMatchday = ScheduledMatch.objects.latest("match_day").match_day
        else:
            showMatchday = untilMatchday

        context = super(PlayerListView, self).get_context_data(**kwargs)
        context["schedules"] = PlayingSchedule.objects.order_by("-season__year")
        matchday_list = (
            ScheduledMatch.objects.filter(
                Q(schedule__playingScheduleId=showSchedule)
                & ~Q(goal_count_home_team=None)
            )
            .values_list("match_day", flat=True)
            .order_by("-play_time")
        )
        context["matchdays"] = sorted(set(matchday_list), reverse=True)
        context["default_schedule"] = showSchedule
        context["default_matchday"] = showMatchday
        return context


class PlayingScheduleListView(ListView):
    model = ScheduledMatch

    def get_paginate_by(self, queryset):
        self.paginate_by = self.request.GET.get("paginate_by")
        if not self.paginate_by:
            self.paginate_by = 10
        return self.paginate_by

    def get_queryset(self):
        favouriteTeam = self.request.GET.get("favTeamId")
        if not favouriteTeam:
            showTeam = Season.objects.latest("year").favourite_team.teamId
        else:
            showTeam = favouriteTeam

        scheduleId = self.request.GET.get("scheduleId")
        if not scheduleId:
            showSchedule = PlayingSchedule.objects.latest(
                "season__year"
            ).playingScheduleId
        else:
            showSchedule = scheduleId

        queryset = ScheduledMatch.objects.filter(
            Q(schedule__playingScheduleId=showSchedule)
            & (Q(home_team_id=showTeam) | Q(guest_team_id=showTeam))
        ).order_by("-play_time")

        return queryset

    def get_context_data(self, **kwargs):
        favouriteTeam = self.request.GET.get("favTeamId")
        if not favouriteTeam:
            showTeam = Season.objects.latest("year").favourite_team
        else:
            showTeam = Team.objects.filter(Q(teamId=favouriteTeam))[0]

        scheduleId = self.request.GET.get("scheduleId")
        if not scheduleId:
            showSchedule = PlayingSchedule.objects.latest(
                "season__year"
            ).playingScheduleId
        else:
            showSchedule = scheduleId

        context = super(PlayingScheduleListView, self).get_context_data(**kwargs)
        context["schedules"] = PlayingSchedule.objects.all()
        context["teams"] = Team.objects.all()
        context["showTeam"] = showTeam.teamId
        context["default_schedule"] = showSchedule
        return context


class ReportDetailView(DetailView):
    model = MatchReport


class StatsListView(ListView):
    model = Team

    def get_queryset(self):
        favouriteTeam = self.request.GET.get("favTeamId")
        if not favouriteTeam:
            showTeam = Season.objects.latest("year").favourite_team.teamId
        else:
            showTeam = favouriteTeam

        scheduleId = self.request.GET.get("scheduleId")
        if not scheduleId:
            showSchedule = PlayingSchedule.objects.latest(
                "season__year"
            ).playingScheduleId
        else:
            showSchedule = scheduleId

        queryset = (
            Team.objects.annotate(
                matchesWonHome=SubqueryCount(
                    "homeTeam",
                    filter=Q(goal_count_home_team__gt=F("goal_count_guest_team"))
                    & Q(schedule__playingScheduleId=showSchedule),
                )
            )
            .annotate(
                shotGoals=SubquerySum(
                    "homeTeam__goal_count_home_team",
                    filter=Q(schedule__playingScheduleId=showSchedule),
                )
                + SubquerySum(
                    "guestTeam__goal_count_guest_team",
                    filter=Q(schedule__playingScheduleId=showSchedule),
                )
            )
            .annotate(
                guestGoals=SubquerySum(
                    "guestTeam__goal_count_guest_team",
                    filter=Q(schedule__playingScheduleId=showSchedule),
                )
            )
            .annotate(
                matchesLost=SubqueryCount(
                    "homeTeam",
                    filter=Q(goal_count_home_team__lt=F("goal_count_guest_team"))
                    & Q(schedule__playingScheduleId=showSchedule),
                )
                + SubqueryCount(
                    "guestTeam",
                    filter=Q(goal_count_guest_team__lt=F("goal_count_home_team"))
                    & Q(schedule__playingScheduleId=showSchedule),
                )
            )
        )
        return queryset

    def get_context_data(self, **kwargs):
        favouriteTeam = self.request.GET.get("favTeamId")
        if not favouriteTeam:
            showTeam = Season.objects.latest("year").favourite_team
        else:
            showTeam = Team.objects.filter(Q(teamId=favouriteTeam))[0]

        scheduleId = self.request.GET.get("scheduleId")
        if not scheduleId:
            showSchedule = PlayingSchedule.objects.latest(
                "season__year"
            ).playingScheduleId
        else:
            showSchedule = scheduleId

        cht_goals = GoalsPerDayLineChart(
            scheduleId=showSchedule,
            height=540,
            width=1200,
            dots_size=4,
            interpolate="hermite",
            interpolation_parameters={
                "type": "kochanek_bartels",
                "b": -1,
                "c": 1,
                "t": 1,
            },
            style=CleanStyle(
                font_family="googlefont:Montserrat",
                background="#f1f1f1",
            ),
        )

        context = super(StatsListView, self).get_context_data(**kwargs)
        context["schedules"] = PlayingSchedule.objects.order_by("-season__year")
        context["teams"] = Team.objects.all()
        context["showTeam"] = showTeam.teamId
        context["default_schedule"] = showSchedule
        context["cht_goals"] = cht_goals.generate()

        matchesCount = ScheduledMatch.objects.filter(
            Q(schedule__playingScheduleId=showSchedule)
            & ~Q(
                goal_count_home_team=None,
            )
        ).count()
        matchesWonHomeAbs = ScheduledMatch.objects.filter(
            Q(schedule__playingScheduleId=showSchedule)
            & Q(goal_count_home_team__gt=F("goal_count_guest_team"))
        ).count()

        matchesWonHomeRel = format(matchesWonHomeAbs / matchesCount * 100, ".2f")

        matchesWonGuestAbs = ScheduledMatch.objects.filter(
            Q(schedule__playingScheduleId=showSchedule)
            & Q(goal_count_home_team__lt=F("goal_count_guest_team"))
        ).count()

        matchesWonGuestRel = format(matchesWonGuestAbs / matchesCount * 100, ".2f")

        matchesDrawAbs = ScheduledMatch.objects.filter(
            Q(schedule__playingScheduleId=showSchedule)
            & Q(goal_count_home_team=F("goal_count_guest_team"))
        ).count()

        matchesDrawRel = format(matchesDrawAbs / matchesCount * 100, ".2f")

        context["matches"] = matchesCount
        context["matchesWonHomeAbs"] = matchesWonHomeAbs
        context["matchesWonHomeRel"] = matchesWonHomeRel
        context["matchesWonGuestAbs"] = matchesWonGuestAbs
        context["matchesWonGuestRel"] = matchesWonGuestRel
        context["matchesDrawAbs"] = matchesDrawAbs
        context["matchesDrawRel"] = matchesDrawRel

        goalsHome = ScheduledMatch.objects.filter(
            Q(schedule__playingScheduleId=showSchedule)
        ).aggregate(Sum("goal_count_home_team"))

        goalsGuest = ScheduledMatch.objects.filter(
            Q(schedule__playingScheduleId=showSchedule)
        ).aggregate(Sum("goal_count_guest_team"))

        goalsPerMatch = (
            goalsHome["goal_count_home_team__sum"]
            + goalsGuest["goal_count_guest_team__sum"]
        ) / matchesCount
        goalsHomePerMatchAbs = goalsHome["goal_count_home_team__sum"]
        goalsHomePerMatchRel = format(
            goalsHome["goal_count_home_team__sum"] / matchesCount / goalsPerMatch * 100,
            ".2f",
        )
        goalsGuestPerMatchAbs = goalsGuest["goal_count_guest_team__sum"]
        goalsGuestPerMatchRel = format(
            goalsGuest["goal_count_guest_team__sum"]
            / matchesCount
            / goalsPerMatch
            * 100,
            ".2f",
        )

        context["goalsPerMatch"] = format(
            goalsPerMatch,
            ".2f",
        )
        context["goalsHomePerMatchAbs"] = goalsHomePerMatchAbs
        context["goalsHomePerMatchRel"] = goalsHomePerMatchRel
        context["goalsGuestPerMatchAbs"] = goalsGuestPerMatchAbs
        context["goalsGuestPerMatchRel"] = goalsGuestPerMatchRel

        games = ScheduledMatch.objects.filter(
            Q(schedule__playingScheduleId=showSchedule) & ~Q(goal_count_guest_team=None)
        )

        highestDifferenceGame = None
        for game in games:
            if not highestDifferenceGame:
                highestDifferenceGame = game
            elif abs(game.goal_count_home_team - game.goal_count_guest_team) > abs(
                highestDifferenceGame.goal_count_home_team
                - highestDifferenceGame.goal_count_guest_team
            ):
                highestDifferenceGame = game
            elif abs(game.goal_count_home_team - game.goal_count_guest_team) == abs(
                highestDifferenceGame.goal_count_home_team
                - highestDifferenceGame.goal_count_guest_team
            ):
                if (game.goal_count_home_team + game.goal_count_guest_team) > (
                    highestDifferenceGame.goal_count_home_team
                    + highestDifferenceGame.goal_count_guest_team
                ):
                    highestDifferenceGame = game

        context["highestDifferenceGame"] = highestDifferenceGame

        return context


class ContactView(FormView):
    template_name = "svh/contact.html"
    form_class = ContactForm
    success_url = reverse_lazy("svh:success")

    def form_valid(self, form):
        # Calls the custom send method
        form.send()
        return super().form_valid(form)


class ContactSuccessView(TemplateView):
    template_name = "svh/success.html"
